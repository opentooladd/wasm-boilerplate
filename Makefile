CRATE_NAME = $(notdir $(CURDIR))

all: build build-web

build: ~/.cargo/bin/wasm-pack ~/.cargo/bin/rustup
	wasm-pack build

build-web: www
	cd www && npm install && npm run build-crate -- $(CRATE_NAME) && npm install

serve-web: www
	cd www && npm run start

clean:
	rm -rf ./www ./pkg

test-all: test-rust test-web test-js

test-rust: pkg www 
	cargo test

test-web: pkg www
	wasm-pack test --firefox --headless

test-js: pkg www
	cd www && npm run test:unit && npm run test:e2e

install-rust:
ifeq ($(OS), Windows_NT)
	echo "Install Rust by downloading https://static.rust-lang.org/rustup/rustup-init.sh"
else
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
endif

install-wasm-pack:
ifeq ($(OS), Windows_NT)
	echo "Install Wasm-Pack by downloading https://github.com/rustwasm/wasm-pack/releases/download/v0.8.1/wasm-pack-init.exe"
else
	curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
endif

install-cargo-edit: ~/.cargo/bin/cargo-add

www:
	git clone https://gitlab.com/opentooladd/wasm-app-boilerplate www && rm -rf www/.git

~/.cargo/bin/wasm-pack:
	make install-wasm-pack

~/.cargo/bin/rustup:
	make install-rust

~/.cargo/bin/cargo-add: ~/.cargo/bin/rustup
	cargo install cargo-edit
